import React, {useEffect,useState, Fragment}  from 'react';

const Lista = () => {

    const [valor,setValores] = useState([])
    const [hasError,setHasError] = useState(false)



    useEffect(() => {
        //console.log('useEffect');
        obtenerValores()
    },[]);

    const obtenerValores = async () => {
        const data = await fetch('https://jsonplaceholder.typicode.com/users')
        //const data = await fetch('https://mindicador.cl/api')
        //const data = await fetch('../json/indicadores.json')
        //console.log('data : '+data);
        const valores = await data.json()
        console.log(valores);
        setValores(valores)
    }
    
    return ( 
        <Fragment>
            <h1>Lista</h1>
            <ul>
                {
                    valor.map((item,index) => (
                        <li key={index}>{item.id} - {item.name} - {item.email}</li>
                       //<li key={index}>{item}</li>
                    ))
                }
            </ul>
        </Fragment>
     );
}
 
export default Lista;