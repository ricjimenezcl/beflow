var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json())
const env = require('./src/config/env.js');
 
require('./src/router/router.js')(app);

const db = require('./src/config/db.config.js');

var server = app.listen(8080, function () {
 
    var host = env.host
    var port = server.address().port
  
    console.log('HOST; '+host);
   
    console.log("App listening at http://%s:%s", host, port)
  })