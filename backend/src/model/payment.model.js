module.exports = (sequelize, Sequelize) => {
	const Payment = sequelize.define('payments', {
    id: {
	 type: Sequelize.INTEGER,
          primaryKey: true
	  },
	  object: {
		  type: Sequelize.STRING
	  },
	  description: {
		  type: Sequelize.STRING
	  },
	  billed_hours: {
		  type: Sequelize.FLOAT
	  },
      billed_at: {
        type: Sequelize.STRING
    },
    billing_currency: {
        type: Sequelize.STRING
    },
    billed_amount: {
        type: Sequelize.FLOAT
    },
    amount: {
        type: Sequelize.INTEGER
    },
    currency: {
        type: Sequelize.STRING
    },
    needs_exchange: {
        type: Sequelize.BOOLEAN
    },
    echange_currency: {
        type: Sequelize.STRING
    },
    exchange: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.STRING
    },
    updated_at: {
        type: Sequelize.STRING
    }
	});
	
	return Payment;
}