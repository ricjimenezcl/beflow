
module.exports = function(app) {

    const paymentController = require('../controller/paymentController.js');   


	app.get('/payment', paymentController.getPayment);

	app.get('/paymentById', paymentController.getPaymentById);

	app.post('/payment', paymentController.createPayment);

	app.post('/updatepayment', paymentController.updatePayment);

	app.post('/payment/deletepayment', paymentController.deletePayment);

}