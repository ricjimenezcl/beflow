var moment = require('moment');
var axios = require('axios');

const db = require('../config/db.config.js');
const Payment = db.payment;




exports.getPayment = async (req, res) => {
   try {
      
   const paymentModel = await Payment.findAll();
if(!paymentModel){
   return res.status(200).send({
     status: 404,
     message: 'No data found'
});
}
return res.json(paymentModel);
   }catch (err) {
      console.error(`Error tabla payments`);
    }
}

exports.getPaymentById = async (req, res) => {
   try {
   //const id= req.body.id;
   const paymentModel = await Payment.findOne({
   where: { code: req.body.id }
});
if(!paymentModel){
   return res.status(200).send({
     status: 404,
     message: 'No data found'
});
}
return res.json(paymentModel);
   }catch (err) {
      console.error(`Error tabla payments`);
    }
}


exports.createPayment = async (req, res) => {

   var  totalUf =  0;
   var campoExchange = '';
  
   if(req.body.needs_exchange){
      
      valorUfDia = await valorUf(req.body.billed_amount);
      var billedAmount = parseFloat(req.body.billed_amount);
      var totalUf = billedAmount * parseFloat(valorUfDia);
      console.log('Resultado UF : '+totalUf);
      campoExchange = ('{"original_amount :"'+ req.body.billed_amount +'","'+'"currency": "clf","exchange_rate":"'+valorUfDia+'"}')
      console.log('campoExchange : '+campoExchange);

   }
   
    const paymentModel =await Payment.build({
         id: req.body.id,
         object: 'pago' ,
         description: req.body.description  ,
         billed_hours: req.body.billed_hours ,
         billed_at: req.body.billed_at  ,
         billing_currency: req.body.billing_currency  ,
         billed_amount: req.body.billed_amount ,
         amount: totalUf,
         currency:'clf',
         needs_exchange: req.body.needs_exchange,
         echange_currency: req.body.echange_currency  ,
         exchange: campoExchange  ,
         created_at: moment().format('DD-MM-YYYY') 
 });
 await paymentModel.save()
 if(!paymentModel){
    return res.status(200).send({
      status: 404,
      message: 'No data found'
 });
 }
 res.status(200).send({
    status: 200,
    message: 'Data Save Successfully'
 });
 }


 exports.updatePayment = async (req, res) => {

   var  totalUf =  0;
   var campoExchange = '';
  
   if(req.body.needs_exchange){
      
      valorUfDia = await valorUf(req.body.billed_amount);
      var billedAmount = parseFloat(req.body.billed_amount);
      var totalUf = billedAmount * parseFloat(valorUfDia);
      campoExchange = ('{"original_amount :"'+ req.body.billed_amount +'","'+'"currency": "clf","exchange_rate":"'+valorUfDia+'"}')

   }

   const paymentModel =await Payment.update({
        object: 'pago' ,
        description: req.body.description  ,
        billed_hours: req.body.billed_hours ,
        billed_at: req.body.billed_at  ,
        billing_currency: req.body.billing_currency  ,
        billed_amount: req.body.billed_amount ,
        amount: totalUf,
        needs_exchange: req.body.needs_exchange,
        echange_currency: req.body.echange_currency  ,
        exchange: campoExchange ,
        updated_at: moment().format('DD-MM-YYYY') 
},
{where: {code: req.body.id} });
if(!paymentModel){
   return res.status(200).send({
     status: 404,
     message: 'No data found'
});
}
res.status(200).send({
   status: 200,
   message: 'payment sucessfully update'
});
}


exports.deletePayment = async (req, res) => {
   try {
   //const id= req.body.id;
   const paymentModel = await Payment.destroy({
   where: { code: req.body.id }
});
if(!paymentModel){
   return res.status(200).send({
     status: 404,
     message: 'No data found'
});
}
res.status(200).send({
   status: 200,
   message: 'payment sucessfully deleted '
});
   }catch (err) {
      console.error(`Error tabla payments`);
    }
}

/*function valorUf(billed_amount){
         const date = moment().format('DD-MM-YYYY');
         console.log('Fecha de UF : ' + date)
         var billedAmount = parseFloat(billed_amount);
         console.log("billed_amount: "+billedAmount);
         var https = require('https');
         https.get(`https://mindicador.cl/api/uf/${date}`, function(res) {
         res.setEncoding('utf-8');
         var data = '';
      
         res.on('data', function(chunk) {
            data += chunk;

         /////res.on('end', function() {
        /////var dailyIndicators = JSON.parse(data); // JSON to JavaScript object
        ////res.send('El valor actual de la UF es $' + dailyIndicators.uf.valor);
        ////});
         var resJs = JSON.stringify(data);
         console.log('retorno valor resJs : '+resJs)
        // console.log("Extraída: ", extraida);
         let indice = resJs.indexOf("valor");
         indice = indice + 7;
         console.log("indice: "+indice);
         var totValorUf = resJs.substring(indice, 205);
         var resultValorUf = parseFloat(totValorUf);
         console.log("Extraída: "+resultValorUf);
         

         const totalUf = billed_amount * totValorUf;
         
         console.log('retorno valor dataxx : '+JSON.stringify(data));

         console.log('total final : '+totalUf);
           
         return totalUf;
         });

 
      
      
      }).on('error', function(err) {
         console.log('Error al consumir la API!');
      });
}*/


async function valorUf(billed_amount){
   const date = moment().format('DD-MM-YYYY');
   console.log('Fecha de UF : ' + date)
   
   const { data } = await axios.get(`https://mindicador.cl/api/uf/${date}`);

   var resJs = JSON.stringify(data);

   let indice = resJs.indexOf("valor");
         indice = indice + 7;
         var totValorUf = resJs.substring(indice, 205);
         var resultValorUf = parseFloat(totValorUf);
         return resultValorUf
   

}